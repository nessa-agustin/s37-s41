e-Commerce API 
==============
Zuitt FullStack Web Developer Bootcamp
Capstone 2

Overview
---------
A backend MVP program to get started with an e-Commerce site.


EndPoints
---------
**Create an account**

_HttpMethod:_ POST `~/users/register`



**Login**

_HttpMethod:_ POST `~/users/login`



**Retrieve User Profile**

_HttpMethod:_ GET   `~/users/{id}`



**Retrieve All Users**

_HttpMethod:_ GET   `~/users`



**Set admin access**

_HttpMethod:_ PUT   `~/users/setadmin`


	
**Add products**

_HttpMethod:_ POST  `~/products`


	
**Retrieve all active products**

_HttpMethod:_ GET   `~/products`


	
**Retrieve all products**

_HttpMethod:_ GET   `~/products/all`


	
**Retrieve specific products**

_HttpMethod:_ GET   `~/products/{id}`


	
**Update specific product**

_HttpMethod:_ GET   `~/products/{id}/update`


	
**Archive specific product**

_HttpMethod:_ GET   `~/products/{id}/archive`


	
**Create order**

_HttpMethod:_ POST  `~/orders`


	
**Retrieve orders**

_HttpMethod:_ GET   `~/orders`


