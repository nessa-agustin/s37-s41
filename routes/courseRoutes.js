const express = require('express')
const router = express.Router();
const courseController = require('../controllers/courseController')
const auth = require("../auth")

// Routes

router.post('/',auth.verify,(req,res) => {

    const userData = auth.decode(req.headers.authorization)
    console.log(userData)

    // if(userData.isAdmin){
    //     courseController.addCourse(req.body)
    //     .then(resultFromController => res.send(resultFromController))
    // }else{
    //     res.status(401).send("You are not authorized to access this resource");
    // }

    courseController.addCourse(req.body,{userId: userData.id,isAdmin: userData.isAdmin})
        .then(resultFromController => res.send(resultFromController))


})

router.get('/all',auth.verify,(req,res) => {

    const userData = auth.decode(req.headers.authorization)
    console.log(userData)

    courseController.getAllCourses(userData)
        .then(resultFromController => {res.send(resultFromController)},
        error => {
            console.log(error);
            res.status(401).send("Resource not allowed")
        }
        )
        

})

router.get('/',(req,res) => {

    courseController.getAllActive()
        .then(resultFromController => res.send(resultFromController))
})


router.get('/:courseId',(req,res) => {

    console.log(req.params.courseId)

    courseController.getCourse(req.params)
        .then(resultFromController => res.send(resultFromController))
})


router.put('/:courseId',auth.verify, (req,res) => {

    const isAdminData = auth.decode(req.headers.authorization).isAdmin;
    console.log(isAdminData)

    courseController.updateCourse(req.params, req.body, isAdminData)
        .then(resultFromController => res.send(resultFromController))
})

router.put('/:courseId/archive',auth.verify, (req,res) => {

    const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    courseController.archiveCourse(req.params.courseId, isAdmin)
        .then(resultFromController => res.send(resultFromController))
})


module.exports = router