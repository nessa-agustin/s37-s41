const Course = require('../models/Course')

module.exports.addCourse = (reqBody, userData) => {

    if(!userData.isAdmin){
        return "You are not authorized to access this resource"
    }else{
        let newCourse = new Course({
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price
        })

        return newCourse.save()
            .then((course, error) => {
                if(error){
                    return false
                }else{
                    return true
                }
            })  
    }

}

module.exports.getAllCourses = (data) => {

    if(data.isAdmin){
        return Course.find({})
            .then(result => {
                return result
            })
    }else{
        return false
    }

        //  return Course.find({})
        //     .then(result => {
        //         return result
        //     })


}


module.exports.getAllActive = () => {

    return Course.find({isActive: true})
        .then(result => {
            return result
        })

}

module.exports.getCourse = (reqParams) => {
    return Course.findById(reqParams.courseId)
        .then(result => {
            return result
        })
}

module.exports.updateCourse = (reqParams, reqBody, data) => {

   /*  if(data){
        let updatedCourse = {
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price
        }

        return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
            .then((updatedCourse, error) => {
                if(error){
                    return false
                }else{
                    return true
                }
            })
    }else{
        return "You are not an Admin"
    } */

    let updatedCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }

    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
    .then((updatedCourse, error) => {
        if(error){
            return false
        }else{
            if(!data){
                return "You are not an Admin"
            }else{
                return true
            }
        }
    })

}


module.exports.archiveCourse = async (courseId,isAdmin) => {

    if(!isAdmin){
        return new Promise((resolve,reject) => {resolve("Not Admin")})
    }

    // let course = await Course.findById(courseId);
    // course.isActive = false

    return Course.findByIdAndUpdate(courseId, {isActive: false})
        .then(result => {return true} )
        .catch(error => {return false})


}