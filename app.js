const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes')
const auth = require('./auth')

const app = express();

const port = process.env.PORT || 4000;

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Main URI

app.use("/users",userRoutes);
app.use("/courses",auth.verify,courseRoutes);


// Mongoose Connection
mongoose.connect('mongodb+srv://admin1234:admin1234@zuitt-batch197.fk6bhtr.mongodb.net/s37-s41?retryWrites=true&w=majority',{
    useNewUrlParser: true,
    useUnifiedTopology: true
})

const db = mongoose.connection;

db.on('error', () => console.error('Connection Error'));
db.once('open', () => console.log('Connected to MongoDb'));

app.listen(port, () => console.log(`API is now online at port:${port}`));